# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ding package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-04 20:42+0200\n"
"PO-Revision-Date: 2021-06-20 22:02+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Generator: Poedit 3.0\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "Імя папкі"

#: askRenamePopup.js:42
msgid "File name"
msgstr "Імя файла"

#: askRenamePopup.js:49 desktopManager.js:791
msgid "OK"
msgstr ""

#: askRenamePopup.js:49
msgid "Rename"
msgstr "Перайменаваць"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "Каманда не знойдзена"

#: desktopManager.js:214
msgid "Nautilus File Manager not found"
msgstr "Файлавы менеджар Nautilus не знойдзены"

#: desktopManager.js:215
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"Файлавы менеджар Nautilus з'яўляецца неабходным для працы Desktop Icons NG."

#: desktopManager.js:754
msgid "Clear Current Selection before New Search"
msgstr ""

#: desktopManager.js:793 fileItemMenu.js:369
msgid "Cancel"
msgstr "Скасаваць"

#: desktopManager.js:795
msgid "Find Files on Desktop"
msgstr ""

#: desktopManager.js:860 desktopManager.js:1483
msgid "New Folder"
msgstr "Новая папка"

#: desktopManager.js:864
msgid "New Document"
msgstr "Новы дакумент"

#: desktopManager.js:869
msgid "Paste"
msgstr "Уставіць"

#: desktopManager.js:873
msgid "Undo"
msgstr "Адрабіць"

#: desktopManager.js:877
msgid "Redo"
msgstr "Паўтарыць"

#: desktopManager.js:883
#, fuzzy
msgid "Select All"
msgstr "Вылучыць усё"

#: desktopManager.js:891
msgid "Show Desktop in Files"
msgstr "Паказаць Стол у файлавым менеджары"

#: desktopManager.js:895 fileItemMenu.js:287
msgid "Open in Terminal"
msgstr "Адкрыць у Тэрмінале"

#: desktopManager.js:901
msgid "Change Background…"
msgstr "Змяніць фон..."

#: desktopManager.js:912
#, fuzzy
msgid "Desktop Icons Settings"
msgstr "Настройкі значкоў Стала"

#: desktopManager.js:916
msgid "Display Settings"
msgstr "Настройкі дісплэя"

#: desktopManager.js:1541
msgid "Arrange Icons"
msgstr ""

#: desktopManager.js:1545
msgid "Arrange By..."
msgstr ""

#: desktopManager.js:1554
msgid "Keep Arranged..."
msgstr ""

#: desktopManager.js:1558
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1563
msgid "Sort Home/Drives/Trash..."
msgstr ""

#: desktopManager.js:1569
msgid "Sort by Name"
msgstr ""

#: desktopManager.js:1571
msgid "Sort by Name Descending"
msgstr ""

#: desktopManager.js:1574
msgid "Sort by Modified Time"
msgstr ""

#: desktopManager.js:1577
msgid "Sort by Type"
msgstr ""

#: desktopManager.js:1580
msgid "Sort by Size"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:156
msgid "Home"
msgstr "Хатоўка"

#: fileItem.js:275
msgid "Broken Link"
msgstr ""

#: fileItem.js:276
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: fileItem.js:326
#, fuzzy
msgid "Broken Desktop File"
msgstr "Паказаць Стол у файлавым менеджары"

#: fileItem.js:327
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: fileItem.js:333
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: fileItem.js:334
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: fileItem.js:336
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: fileItem.js:339
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: fileItem.js:347
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: fileItemMenu.js:118
msgid "Open All..."
msgstr "Адкрыць усё..."

#: fileItemMenu.js:118
msgid "Open"
msgstr "Адкрыць"

#: fileItemMenu.js:129
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:129
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:141
msgid "Scripts"
msgstr "Скрыпты"

#: fileItemMenu.js:147
msgid "Open All With Other Application..."
msgstr "Адурыць усё ў іншай праграме..."

#: fileItemMenu.js:147
msgid "Open With Other Application"
msgstr "Адурыць у іншай праграме"

#: fileItemMenu.js:153
msgid "Launch using Dedicated Graphics Card"
msgstr "Запусціць з дапамогай дыскрэтнай відэакарты"

#: fileItemMenu.js:162
msgid "Run as a program"
msgstr ""

#: fileItemMenu.js:170
msgid "Cut"
msgstr "Выразаць"

#: fileItemMenu.js:175
msgid "Copy"
msgstr "Капіяваць"

#: fileItemMenu.js:181
msgid "Rename…"
msgstr "Перайменаваць..."

#: fileItemMenu.js:189
msgid "Move to Trash"
msgstr "Выкінуць у сметніцу"

#: fileItemMenu.js:195
msgid "Delete permanently"
msgstr "Выдаліць назаўжды"

#: fileItemMenu.js:203
msgid "Don't Allow Launching"
msgstr "Не дазваляць выкананне"

#: fileItemMenu.js:203
msgid "Allow Launching"
msgstr "Дазволіць выкананне"

#: fileItemMenu.js:214
msgid "Empty Trash"
msgstr "Сметніца пустая"

#: fileItemMenu.js:225
#, fuzzy
msgid "Eject"
msgstr "Выняць"

#: fileItemMenu.js:231
msgid "Unmount"
msgstr "Адмантаваць"

#: fileItemMenu.js:241
msgid "Extract Here"
msgstr "Выняць сюды"

#: fileItemMenu.js:245
msgid "Extract To..."
msgstr "Выняць у..."

#: fileItemMenu.js:252
msgid "Send to..."
msgstr "Дасладь да..."

#: fileItemMenu.js:258
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Сціснуць {0} файл"
msgstr[1] "Сціснуць {0} файлы"
msgstr[2] "Сціснуць {0} файлаў"

#: fileItemMenu.js:264
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Новая папка з {0} элементам"
msgstr[1] "Новая папка з {0} элементамі"
msgstr[2] "Новая папка з {0} элементамі"

#: fileItemMenu.js:273
msgid "Common Properties"
msgstr "Агульныя ўласцівасці"

#: fileItemMenu.js:273
msgid "Properties"
msgstr "Уласцівасці"

#: fileItemMenu.js:280
msgid "Show All in Files"
msgstr "Паказаць усё ў файлавым менеджары"

#: fileItemMenu.js:280
msgid "Show in Files"
msgstr "Паказаць у файлавым менеджары"

#: fileItemMenu.js:365
msgid "Select Extract Destination"
msgstr "Абраць куды выняць змесціва"

#: fileItemMenu.js:370
msgid "Select"
msgstr "Вылучыць"

#: fileItemMenu.js:396
msgid "Can not email a Directory"
msgstr "Немагчыма даслаць папку"

#: fileItemMenu.js:397
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "Сярод вылучанага ёсць папка, спачатку сцісніце папку ў файл."

#: preferences.js:67
msgid "Settings"
msgstr "Настройкі"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "Памер значкоў працоўнага стала"

#: prefswindow.js:46
msgid "Tiny"
msgstr "Маленькі"

#: prefswindow.js:46
msgid "Small"
msgstr "Малы"

#: prefswindow.js:46
msgid "Standard"
msgstr "Стандартны"

#: prefswindow.js:46
msgid "Large"
msgstr "Вялікі"

#: prefswindow.js:47
msgid "Show the personal folder in the desktop"
msgstr "Паказваць асабістую папку на працоўным стале"

#: prefswindow.js:48
msgid "Show the trash icon in the desktop"
msgstr "Паказваць значок сметніцы на працоўным стале"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "Паказваць зменныя носьбіты на працоўным стале"

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "Паказваць сеткавыя носьбіты на працоўным стале"

#: prefswindow.js:53
msgid "New icons alignment"
msgstr "Новае выраўноўванне значкоў"

#: prefswindow.js:54
msgid "Top-left corner"
msgstr "Верхні левы вугал"

#: prefswindow.js:55
msgid "Top-right corner"
msgstr "Верхні правы вугал"

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr "Ніжні левы вугал"

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr "Ніжні правы вугал"

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Дадаць новыя носьбіты на супрацьлеглы бок экрану"

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Падсвечваць месца перамяшчэння файла падчас Drag'n'Drop"

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr ""

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr ""

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr "Настройкі звязаныя з Nautilus"

#: prefswindow.js:90
msgid "Click type for open files"
msgstr "Від кліку для адкрыцця файлаў"

#: prefswindow.js:90
msgid "Single click"
msgstr "Адзіночны клік"

#: prefswindow.js:90
msgid "Double click"
msgstr "Двайны клік"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "Паказваць схаваныя файлы"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr "Паказваць пункт \"выдаліць назаўжды\" у кантэкстным меню"

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr "Дзеянне пры запуску праграмы з працоўнага стала"

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr "Паказаць змесціва файла"

#: prefswindow.js:99
msgid "Launch the file"
msgstr "Запусціць файл"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "Спытаць што зрабіць"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "Паказваць мініяцюры"

#: prefswindow.js:107
msgid "Never"
msgstr "Ніколі"

#: prefswindow.js:108
msgid "Local files only"
msgstr "Толькі для лакальных файлаў"

#: prefswindow.js:109
msgid "Always"
msgstr "Заўжды"

#: showErrorPopup.js:40
msgid "Close"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "Памер значка"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Наладзіць памер значкоў працоўнага стала."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Паказваць асабістую папку"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Паказваць асабістую папку на працоўным стале."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Паказваць значок сметніцы"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Паказваць значок сметніцы на працоўным стале."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "Вугал экрана для новых значкоў"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Наладзіць вугал з якога будзе пачынацца размяшчэнне значкоў."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Паказваць зменныя носьбіты, падлучаныя да камп'ютара."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "Паказваць прымантаваныя сеткавыя тамы на працоўным стале."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Пры дадаванні носьбітаў і тамоў на стол, размяшчаць іх на супрацьлеглым баку "
"экрана."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Паказваць прамавугольнік у месцы перамяшчэння падчас DnD"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"Падчас аперацыі Drag'n'Drop пазначае месца ў сетцы, у якое будзе перамешчаны "
"элемент, паўпразрыстым прамавугольнікам."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Каб наладзіць Desktop Icons NG, клікніце правай кнопкай мышы па працоўным "
#~ "стале і абярыце апошні пункт: \"Настройкі значкоў стала\""

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "Вы хаціце выканаць “{0}”, ці паказаць яго змесціва?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "“{0}” гэта выканальны тэкставы файл."

#~ msgid "Execute in a terminal"
#~ msgstr "Выканаць у тэрмінале"

#~ msgid "Show"
#~ msgstr "Паказаць"

#~ msgid "Execute"
#~ msgstr "Выканаць"

#~ msgid "Delete"
#~ msgstr "Выдаліць"

#~ msgid "Error while deleting files"
#~ msgstr "Памылка падчас выдалення файлаў"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "Ці вы насамрэч хочаце незваротна выдаліць гэтыя элементы?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "Выдаленне элемента незваротна."

#~ msgid "New folder"
#~ msgstr "Новая папка"
